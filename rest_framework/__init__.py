from datetime import datetime
import os

# MySql connection configs:
db_host = 'string'
db_port = int
db_user = 'admin'
db_password = ''
db_name = ''


# Current time to string
cur_t = lambda: (datetime.now().strftime('%H:%M:%S'))

# token type
token_type = 'Bearer '



BASE_DIR = os.path.dirname(os.path.abspath(__file__))
