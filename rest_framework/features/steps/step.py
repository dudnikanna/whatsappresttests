from behave import *
import json
from rest_framework import BASE_DIR
from rest_framework.tools import send_request, compare_response, DB

use_step_matcher("parse")


@when("I make {method} request using data from file: {filename}")
def step_impl(context, method, filename):
    with open("%s/requests/%s" %(BASE_DIR, filename)) as json_data:
        request_data = json.load(json_data)
        context.result = send_request(request_data, method)
        print(context.result)


@then("I receive status code: {status_code}")
def step_impl(context, status_code):
    assert context.result[0] == int(status_code)


@step("I compare saved file: {file} and server response, the value of following keys will not be checked")
def step_impl(context, file):
    result = compare_response(context.result[1], file, context.text)
    try:
        assert result[0] == True
    except AssertionError:
        print('ASSERTION ERROR! One of the possible reasons is:')
        print(result[1])
        raise AssertionError


@given('I run sql script {filename}')
def step_impl(context, filename):
    db = DB()
    with open("%s/sql_scripts/%s" % (BASE_DIR, filename)) as f:
        sql = f.read()
    db.update(sql=sql)


@step("I save access_token of this user")
def step_impl(context):
    token = context.result[1].get('access_token')
    file = open("%s/tools/access_token" % BASE_DIR, "w")
    file.write(token)
    file.close()


@step("I receive {amount} elements in response")
def step_impl(context, amount):
    assert len(context.result[1]) == int(amount)


@step("I compare saved file: {file} and server response, all keys and values should be equal")
def step_impl(context, file):
    result = compare_response(context.result[1], file, [])
    try:
        assert result[0] == True
    except AssertionError:
        print('ASSERTION ERROR! One of the possible reasons is:')
        print(result[1])
        raise AssertionError