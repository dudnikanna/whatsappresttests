# Created by gorbanenko_cr at 27.10.16
Feature: Test Get started mobile

  Scenario: Positive Scenario. Create new user
    Given I run sql script 001_getStarted.sql
    When I make POST request using data from file: 001_request.json
    Then I receive status code: 200
    And I compare saved file: 001_expected.json and server response, the value of following keys will not be checked
        """
        ["id", "access_token"]
        """
    And I save access_token of this user

  Scenario: Positive Scenario. Get started mobile
     When I make POST request using data from file: 002_request.json
    Then I receive status code: 200
    And I compare saved file: 002_expected.json and server response, the value of following keys will not be checked
     """
        ["id", "owner_user_id", "showcase", "owner", "userRoles", "programs", "api_key"]
     """
  Scenario: Some get request
    When I make GET request using data from file: 003_request.json
    Then I receive status code: 200
    And I receive 10 elements in response
    And I compare saved file: 003_expected.json and server response, all keys and values should be equal


   


