from rest_framework.tools import jsoncom
import json
from rest_framework import BASE_DIR


def compare_response(response, file, list_of_ignorred_keys):
    # encoding: utf-8
    with open("%s/expected_responses/%s" % (BASE_DIR, file), encoding='utf-8') as json_data:
        expected = json.load(json_data)
    return jsoncom.are_same(expected, response, False, list_of_ignorred_keys)
