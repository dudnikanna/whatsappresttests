import pymysql
from rest_framework import db_host, db_name, db_password, db_port, db_user, cur_t
import subprocess


class DB(object):
    def __init__(self):
        subprocess.call(["ssh -i /Users/gorbanenko_cr/Documents/ssh/id_rsa -fNL 9870:db1:3306 dev@feature.zipcap.com "], shell=True)
        print("%s Creating MySQL connection..." % cur_t())
        self._connection = pymysql.connect(host=db_host,
                                           port=db_port,
                                           user=db_user,
                                           password=db_password,
                                           db=db_name,
                                           charset='utf8mb4',
                                           cursorclass=pymysql.cursors.DictCursor)

    def select(self, sql):
        print("%s Executing SQL query: %s" % (cur_t(), sql))
        db = self._connection.cursor()
        db.execute(sql)
        result = []
        row = db.fetchone()
        result.append(row)
        while row is not None:
            row = db.fetchone()
            if row is not None:
                result.append(row)
        return result

    def update(self, sql):
        print("%s Executing SQL query: %s" % (cur_t(), sql))
        db = self._connection.cursor()
        db.execute(sql)
        self._connection.commit()

    def __del__(self):
        self._connection.close()
        print("%s MySQL connection is closed!" % cur_t())
