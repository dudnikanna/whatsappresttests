import requests
import json
from rest_framework import BASE_DIR, token_type


def send_request(request_data, method):
        headers = request_data['headers']
        if 'Authorization' in headers.keys():
            file = open('%s/tools/access_token' % BASE_DIR, 'r')
            token = file.read()
            file.close()
            headers.update({'Authorization': token_type + token})
        method = method.lower()
        if method == 'post':
            r = requests.post(url=request_data['url'], headers=headers,
                              data=json.dumps(request_data['body']))

        elif method == 'put':
            r = requests.put(url=request_data['url'], headers=headers,
                             data=json.dumps(request_data['body']))
        elif method == 'patch':
            r = requests.patch(url=request_data['url'], headers=headers,
                               data=json.dumps(request_data['body']))
        elif method == 'get':
            r = requests.get(url=request_data['url'], headers=headers)
        elif method == 'delete':
            r = requests.delete(url=request_data['url'], headers=headers)
        else:
            return 400, "Unknown Method!"
        return r.status_code, r.json()
