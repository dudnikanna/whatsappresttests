# Created by dudnik_anna_cr at 02.12.16
Feature: Post report, user can't see this event, but other can

  @test_01
  Scenario:  Positive. User send report
    Given I run sql script
    """
    DELETE FROM whats_web_dev.favorites_events WHERE event_id IN (SELECT id FROM `whats_web_dev`.`events` WHERE title = 'Carnaval');
    DELETE FROM whats_web_dev.user_reported_event WHERE event_id IN (SELECT id FROM `whats_web_dev`.`events` WHERE title = 'Carnaval');
    DELETE FROM event_to_sub_category WHERE event_id IN (SELECT id FROM whats_web_dev.events WHERE title = 'Carnaval');
    DELETE FROM whats_web_dev.events WHERE title = 'Carnaval';
    INSERT INTO `whats_web_dev`.`events` (`service_id`,  `title`,`description`,`service_type_id`,`status`,`start_time`,`end_time`,`city`,`street`,`lat`,`lng`,`icon`,`category_id`,`is_deleted`,`url`,`is_blocked`,`original_icon`) VALUES('235569158','Carnaval','description','2','1',NOW(),NOW(),'Dnepr','Komsomolskaya','-7.01','-60.29','http://photos4.meetupstatic.com/photos/event/6/b/0/f/600_447327407.jpeg',(SELECT id FROM whats_web_dev.categories WHERE title = 'Cultural'),'0','http://www.meetup.com/yazamalhabar/events/235459295/','0','https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F24810017%2F152877892302%2F1%2Foriginal.jpg?s=a1d75001c927ee31bd62b8b395c2eed8');
    SET SQL_SAFE_UPDATES = 0;
    UPDATE events SET status = 1 WHERE title = 'Carnaval';
    INSERT INTO `whats_web_dev`.`event_to_sub_category` (`event_id`, `sub_category_id`) VALUES ((SELECT id FROM `whats_web_dev`.`events` WHERE title = 'Carnaval'), (SELECT id FROM whats_web_dev.sub_categories WHERE title = 'Music shows'));
    context.event_id = SELECT id FROM `whats_web_dev`.`events` WHERE title = 'Carnaval';
    """
    And I add request body
    """
    {
    "reasonId": 1
    }
    """
    And I set "Content-Type" header to "application/json"
    And I set "Authorization" header to "context.user_token"
    When I make a POST request to "/event/context.event_id/report"
    Then the response status code should equal 200
     And the response structure should equal "ReportedEvent"
     And JSON at path ".userId" should equal 28
     And JSON at path ".eventId" should equal context.event_id
     And JSON at path ".reasonId" should equal 1


  @test_01
  Scenario:  Positive. User can't see this event
     Given  I set "Content-Type" header to "application/json"
     And I set "Authorization" header to "context.user_token"
    When I make a GET request to "/events"
     | PARAMETER_NAME | VALUE                  |
     | categories     | context.category_id    |
     | subCategories  | context.subcategory_id |
     | period         | today                  |
     | lat            | -7.01                  |
     | lng            | -60.29                 |
     Then the response status code should equal 200
     And the response structure should equal "GetEvent"
     And JSON at path ".totalCount" should equal 0


  @test_01
  Scenario:  Other users can see reported event
     Given I set "Content-Type" header to "application/json"
     And I set "Authorization" header to "context.admin_token"
    When I make a GET request to "/events"
     | PARAMETER_NAME | VALUE                  |
     | categories     | context.category_id    |
     | subCategories  | context.subcategory_id |
     | period         | today                  |
     | lat            | -7.01                  |
     | lng            | -60.29                 |
     Then the response status code should equal 200
     And the response structure should equal "GetEvent"
     And JSON at path ".totalCount" should equal 1


