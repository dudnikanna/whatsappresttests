# Created by dudnik_anna_cr at 25.11.16
Feature: Get categories

   @test_01
  Scenario: Get categories
    Given I set "Content-Type" header to "application/json"
    When I make a GET request to "/categories"
    Then the response status code should equal 200
    And the response structure should equal "Categories"

