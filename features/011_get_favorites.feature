# Created by dudnik_anna_cr at 02.12.16
  Feature: User get favorites events

@test_01
  Scenario:  Positive. User get favorites events. Check last event info
     Given I set "Content-Type" header to "application/json"
    And I set "Authorization" header to "context.user_token"
    When I make a GET request to "/user/favorite-events"
    Then the response status code should equal 200
     And the response structure should equal "FavoritesList"