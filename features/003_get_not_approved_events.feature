# Created by dudnik_anna_cr at 25.11.16
Feature: Get not-approved

   @test_01
  Scenario: Positive. Admin gets not approved events
    Given I set "Content-Type" header to "application/json"
      And I set "Authorization" header to "context.admin_token"
    When I make a GET request to "/events/not-approved"
    Then the response status code should equal 200
     And the response structure should equal "EventsListNotApproved"



   @test_01
  Scenario: Negative. User gets not approved events
    Given I set "Content-Type" header to "application/json"
     And I set "Authorization" header to "context.user_token"
    When I make a GET request to "/events/not-approved"
    Then the response status code should equal 405


