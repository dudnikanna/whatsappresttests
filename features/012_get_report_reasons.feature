# Created by dudnik_anna_cr at 02.12.16
Feature: Get report reasons

   @test_01
  Scenario: Positive. Admin gets report reasons
    Given I set "Content-Type" header to "application/json"
      And I set "Authorization" header to "context.admin_token"
    When I make a GET request to "/report-reasons"
    Then the response status code should equal 200
     And the response structure should equal "ReportReasons"
     And JSON at path ".[0].id" should equal 1
     And JSON at path ".[0].text" should equal "Adult"
     And JSON at path ".[1].id" should equal 2
     And JSON at path ".[1].text" should equal "Insult"
     And JSON at path ".[2].id" should equal 3
     And JSON at path ".[2].text" should equal "Spam"


   @test_01
  Scenario: Positive. User gets report reasons
    Given I set "Content-Type" header to "application/json"
     And I set "Authorization" header to "context.user_token"
    When I make a GET request to "/report-reasons"
    Then the response status code should equal 200
     And the response structure should equal "ReportReasons"
     And JSON at path ".[0].id" should equal 1
     And JSON at path ".[0].text" should equal "Adult"
     And JSON at path ".[1].id" should equal 2
     And JSON at path ".[1].text" should equal "Insult"
     And JSON at path ".[2].id" should equal 3
     And JSON at path ".[2].text" should equal "Spam"
