# Created by dudnik_anna_cr at 02.12.16
Feature: Get reported events list

  @test_01
  Scenario:  Positive. Admin gets reported events list
    Given I set "Content-Type" header to "application/json"
    And I set "Authorization" header to "context.admin_token"
    When I make a GET request to "/events/reported"
    Then the response status code should equal 200
    And the response structure should equal "ReportedEvents"


  @test_01
  Scenario:  Negative. User try to get reported events list
    Given I set "Content-Type" header to "application/json"
    And I set "Authorization" header to "context.user_token"
    When I make a GET request to "/events/reported"
    Then the response status code should equal 405



