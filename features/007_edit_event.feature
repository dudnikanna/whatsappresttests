# Created by dudnik_anna_cr at 01.12.16
Feature: Edit events

   @test_01
  Scenario:  Positive. Admin edit event
     Given I run sql script
     """
     DELETE FROM whats_web_dev.favorites_events WHERE event_id IN (SELECT id FROM `whats_web_dev`.`events` WHERE title = 'Carnaval');
     DELETE FROM whats_web_dev.user_reported_event WHERE event_id IN (SELECT id FROM `whats_web_dev`.`events` WHERE title = 'Carnaval');
     DELETE FROM event_to_sub_category WHERE event_id IN (SELECT id FROM `whats_web_dev`.`events` WHERE title = 'Carnaval' );
     DELETE FROM whats_web_dev.events WHERE title = 'Carnaval';
     INSERT INTO `whats_web_dev`.`events` (`service_id`,  `title`,`description`,`service_type_id`,`status`,`start_time`,`end_time`,`city`,`street`,`lat`,`lng`,`icon`,`category_id`,`is_deleted`,`url`,`is_blocked`,`original_icon`)
     VALUES('235569158','Carnaval','description','2','0',NOW(),NOW(),'Dnepr','Komsomolskaya','-7.01','-60.29','http://photos4.meetupstatic.com/photos/event/6/b/0/f/600_447327407.jpeg',(SELECT id FROM whats_web_dev.categories WHERE title = 'Cultural'),'0','http://www.meetup.com/yazamalhabar/events/235459295/','0','https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F24810017%2F152877892302%2F1%2Foriginal.jpg?s=a1d75001c927ee31bd62b8b395c2eed8');
     context.event_id = SELECT id FROM `whats_web_dev`.`events` WHERE title = 'Carnaval';
     """
     And I set "Content-Type" header to "application/json"
     And I set "Authorization" header to "context.admin_token"
     And I set "icon" header to "https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F24810017%2F152877892302%2F1%2Foriginal.jpg?s=a1d75001c927ee31bd62b8b395c2eed8"
     And I add request body
     """
     {
     "title": "new title",
     "description": "new description",
     "city": "new city",
     "street": "new street",
     "icon": "https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F25340417%2F14641422515%2F1%2Foriginal.jpg?s=1bea710d157cba77f2e89b35214a9549"
     }
     """
    When I make a PATCH request to "/event/context.event_id"
     Then the response status code should equal 200
     And the response structure should equal "EditedEvent"
     And JSON at path ".title" should equal "new title"
     And JSON at path ".description" should equal "new description"
     And JSON at path ".city" should equal "new city"
     And JSON at path ".street" should equal "new street"
     And JSON at path ".icon" should equal "https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F25340417%2F14641422515%2F1%2Foriginal.jpg?s=1bea710d157cba77f2e89b35214a9549"

   @test_01
  Scenario:  Negative. User can't edit event
     Given I set "Content-Type" header to "application/json"
     And I set "Authorization" header to "context.user_token"
     And I set "icon" header to "https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F24810017%2F152877892302%2F1%2Foriginal.jpg?s=a1d75001c927ee31bd62b8b395c2eed8"
     And I add request body
     """
     {
     "title": "new title",
     "description": "new description",
     "city": "new city",
     "street": "new street",
     "icon": "https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F25340417%2F14641422515%2F1%2Foriginal.jpg?s=1bea710d157cba77f2e89b35214a9549"
     }
     """
    When I make a PATCH request to "/event/context.event_id"
     Then the response status code should equal 405

