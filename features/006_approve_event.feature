# Created by dudnik_anna_cr at 28.11.16
Feature: Approved events

  @test_01
  Scenario:  Positive. Admin approve event
    Given I run sql script
    """
    DELETE FROM whats_web_dev.favorites_events WHERE event_id IN (SELECT id FROM `whats_web_dev`.`events` WHERE title = 'Carnaval');
    DELETE FROM whats_web_dev.user_reported_event WHERE event_id IN (SELECT id FROM `whats_web_dev`.`events` WHERE title = 'Carnaval');
    DELETE FROM event_to_sub_category WHERE event_id IN (SELECT id FROM `whats_web_dev`.`events` WHERE title = 'Carnaval' );
    DELETE FROM whats_web_dev.events WHERE title = 'Carnaval';
    INSERT INTO `whats_web_dev`.`events` (`service_id`,  `title`,`description`,`service_type_id`,`status`,`start_time`,`end_time`,`city`,`street`,`lat`,`lng`,`icon`,`category_id`,`is_deleted`,`url`,`is_blocked`,`original_icon`)
    VALUES('235569158','Carnaval','description','2','0',NOW(),NOW(),'Dnepr','Komsomolskaya','-7.01','-60.29','http://photos4.meetupstatic.com/photos/event/6/b/0/f/600_447327407.jpeg',(SELECT id FROM whats_web_dev.categories WHERE title = 'Cultural'),'0','http://www.meetup.com/yazamalhabar/events/235459295/','0','https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F24810017%2F152877892302%2F1%2Foriginal.jpg?s=a1d75001c927ee31bd62b8b395c2eed8');
    context.category_id = SELECT id FROM whats_web_dev.categories WHERE title = 'Cultural';
    context.subcategory_id = SELECT id FROM whats_web_dev.sub_categories WHERE title = 'Music shows';
    context.event_id = SELECT id FROM `whats_web_dev`.`events` WHERE title = 'Carnaval';
    """
    And I set "Content-Type" header to "application/json"
    And I set "Authorization" header to "context.admin_token"
    And I add request body
    """
    {
    "categoryId": "context.category_id",
    "subCategories":
    [
    "context.subcategory_id"
    ]
    }
    """
    When I make a PATCH request to "/event/context.event_id/approve"
    Then the response status code should equal 200
     And the response structure should equal "ApprovedEvent"
     And JSON at path ".id" should equal context.event_id
     And JSON at path ".title" should equal "Carnaval"
     And JSON at path ".description" should equal "description"
     And JSON at path ".icon" should equal "http://photos4.meetupstatic.com/photos/event/6/b/0/f/600_447327407.jpeg"
     And JSON at path ".originalIcon" should equal "https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F24810017%2F152877892302%2F1%2Foriginal.jpg?s=a1d75001c927ee31bd62b8b395c2eed8"
     And JSON at path ".startTime" should similar current_date
     And JSON at path ".endTime" should similar current_date
     And JSON at path ".city" should equal "Dnepr"
     And JSON at path ".street" should equal "Komsomolskaya"
     And JSON at path ".lat" should equal -7.01
     And JSON at path ".lng" should equal -60.29
     And JSON at path ".url" should equal "http://www.meetup.com/yazamalhabar/events/235459295/"


  @test_02
  Scenario:  Negative. User try to approve event
  Given I set "Content-Type" header to "application/json"
    And I set "Authorization" header to "context.user_token"
    And I add request body
    """
    {
    "categoryId": "context.category_id",
    "subCategories":
    [
    135
    ]
    }
    """
    When I make a PATCH request to "/event/context.event_id/approve"
    Then the response status code should equal 405