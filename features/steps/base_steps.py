from behave import *
import requests
import json
import nose
from features.steps.sql_unit import DB
import dateutil.parser
import datetime
from features.steps.tools import *
import pprint


use_step_matcher("parse")


@step('I make a {http_verb} request to "{url_path_segment}"')
def make_a_request(context, http_verb, url_path_segment):
    if 'context.' in url_path_segment:
        url_path_segment = build_url_from_path_segment(context, url_path_segment)
    if context.table:
        param_part = build_url_from_table_params(context)
    else:
        param_part = ''
    url = context.base_url + url_path_segment + param_part
    print('\nRequest url is: %s' % url)
    if http_verb == "GET" or http_verb == "DELETE":
        context.r = getattr(requests, http_verb.lower())(url, headers=context.headers)
    else:
        context.r = getattr(requests, http_verb.lower())(url, headers=context.headers, data=json.dumps(context.body))
    log_full(context.r)
    return context.r


@step("I add request body")
def add_body_to_request(context):
    try:
        context.body = json.loads(context.text)
        context.body = replace_if_need(context, context.body)
    except Exception as e:
        raise ScenarioIsFailed(log_error(' Exception! Cannot convert body to json %s \n' % e))
    print('---------- BODY IS ADDED ----------')
    pprint.pprint(context.body, indent=4)
    print('\n')


@step("the response status code should equal {expected_code}")
def validate_status_code(context, expected_code):
    nose.tools.assert_equal(context.r.status_code, int(expected_code))


@step("I run sql script")
def step_impl(context):
    if context.text == "" or None:
        raise ScenarioIsFailed(log_error(' Exception! Cannot read sql script! \n'))

    for part in context.text.split(';'):
        if part.startswith("\n"):
            part = part[1:]
        if part != '':
            part += ';'
            if part.startswith("context"):
               variable = part[8:part.find('=') - 1]
               context.sql = part[part.find('=')+1:]
               db = DB()
               dict = db.select(context.sql)[0]
               for key in dict.keys():
                   value = dict.get(key)
               context.__setattr__(variable, value)
               write_key_value_to_config_file(variable, value)
            else:
               context.sql = replace_context_in_sql(part)
               db = DB()
               db.update(context.sql)


@step('the response header "{header_name}" should equal "{expected_header_value}"')
def headers_parameter_validation(context, header_name, expected_header_value):
    nose.tools.assert_equal(context.r.headers[header_name], str(expected_header_value))


@step('I set "{header_name}" header to "{header_value}"')
def set_header(context, header_name, header_value):
    if header_value.startswith("context"):
        context.headers[header_name] = str(context.__getattr__(header_value[8:]))
    else:
        context.headers[header_name] = header_value


@step('the response structure should equal "{expected_response_structure}"')
def response_structure_validation(context, expected_response_structure):
    data = context.r.json()
    try:
        response_valid = getattr(context.json_responses, expected_response_structure)

        assert response_valid.check(data)
    except NameError:
        print("")
        print("File with responses not found")
        print("")


@step('JSON at path "{json_path}" should equal {expected_json_value}')
def json_object_validation(context, json_path, expected_json_value):
    data = context.r.json()
    actual_json_value = get_part_of_json(data, json_path)
    if expected_json_value.startswith("context"):
        expected_json_value = context.__getattr__(expected_json_value[8:])
        nose.tools.assert_equal(actual_json_value, expected_json_value)
    else:
        try:
            converted_value = json.loads(expected_json_value)
            nose.tools.assert_equal(actual_json_value, converted_value)
        except AssertionError:
            nose.tools.assert_equal(str(actual_json_value), converted_value)

@step('JSON at path "{json_path}" should similar current_date')
def step_impl(context, json_path):
    data = context.r.json()
    actual_json_value = get_part_of_json(data, json_path)
    actual_date = dateutil.parser.parse(actual_json_value).strftime("%Y-%m-%d")
    expected_date = datetime.datetime.now().strftime("%Y-%m-%d")
    nose.tools.assert_equal(actual_date, expected_date)


@step('JSON value at path "{json_path}" I save as "context.{var}"')
def step_impl(context, json_path, var):
    data = context.r.json()
    value = get_part_of_json(data, json_path)
    print(value)
    if var == 'token':
        value = 'Bearer ' + value
    write_key_value_to_config_file(var, value)
    context.__setattr__(var, value)


@step("I compare {val1} and {val2} it should be {is_equal}")
def step_impl(context, val1, val2, is_equal):
    if val1.startswith("context"):
        val1 = context.__getattr__(val1[8:])
    if val2.startswith("context"):
        val2 = context.__getattr__(val2[8:])
    if is_equal == 'equal':
        assert val1 == val2
    elif is_equal == 'not equal':
        assert val1 != val2


