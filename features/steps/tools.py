from behave import *
from features import BASE_DIR
import yaml
import jpath


class ScenarioIsFailed(Exception):
    pass


def log_error(str):
    print("")
    print("")
    print('-----------ERROR-----------')
    print(str)
    print("")
    print("")


def log_full(r):
    req = r.request
    """
    At this point it is completely built and ready
    to be fired; it is "prepared".

    However pay attention at the formatting used in
    this function because it is programmed to be pretty
    printed and may differ from the actual request.
    """

    print("")
    print("")

    print('{}\n{}\n{}\n\n{}'.format(
        '-----------REQUEST-----------',
        req.method + ' ' + req.url,
        '\n'.join('{}: {}'.format(k, v) for k, v in req.headers.items()),
        req.body,
    ))

    print("")

    print('{}\n{}\n{}\n\n{}'.format(
        '-----------RESPONSE-----------',
        str(r.status_code) + ' ' + r.reason,
        '\n'.join('{}: {}'.format(k, v) for k, v in r.headers.items()),
        r.text,
    ))
    print("")

    print('Operation took ' + str(round(r.elapsed.total_seconds(), 3)) + 's')

    print("")
    print("")
    print("")
    print("")

def write_key_value_to_config_file(key, value):
    settings = yaml.load(open(BASE_DIR + '/features/config.yaml').read())
    settings.update({key: value})
    with open(BASE_DIR + '/features/config.yaml', 'w') as yaml_file:
        yaml.dump(settings, yaml_file, default_flow_style=False)


def replace_context_in_sql(sql):
    settings = yaml.load(open(BASE_DIR + '/features/config.yaml').read())
    for key, value in settings.items():
        sql = sql.replace('context.%s' % key, "'%s'" % str(value))
    return sql


def build_url_from_table_params(context):
    params = {}
    for row in context.table:
        param = row['PARAMETER_NAME']
        value = row['VALUE']
        if param.startswith("context"):
            param = eval(param)
        if value.startswith("context"):
            value = eval(value)
        params.update({param: value})
    param_part = '?'
    for key in params.keys():
        param_part = param_part + key + '=' + str(params.get(key)) + '&'
    return param_part[:-1]


def build_url_from_path_segment(context, url_path_segment):
    url_parts = (url_path_segment.split('/'))
    url_parts.pop(0)
    url_path_segment = '/'
    for part in url_parts:
        if part.startswith("context"):
            elem = str(eval(part))
        else:
            elem = part
        url_path_segment += elem + '/'
    return url_path_segment[:-1]


def get_part_of_json(data, json_p):
    path_list = json_p.split('.')
    result = data
    for elem in path_list:
        if elem == '':
            pass
        elif elem.startswith('[') and elem.endswith(']'):
            result = result[int(elem[1:-1])]

        else:
            try:
                result = jpath.get('.' + elem, result)
            except Exception as e:
                print('Key Error %s' % e)
                if type(result) == list:
                    print('object is a list!')
                raise KeyError
    return result


def replace_if_need(context, json):
    for key in json:
        if type(json[key]) == list:
            for element in json[key]:
                if type(element) == dict or type(element) == list:
                    replace_if_need(context, element)
                else:
                    if type(element) == str and element.startswith("context"):
                        json[key].append(context.__getattr__(element[8:]))
                        json[key].remove(element)
                print('Element - %s' % element)
        elif type(json[key]) == dict:
            replace_if_need(context, json[key])
        elif type(json[key]) == str:
            if (json[key]).startswith("context"):
                json[key] = context.__getattr__(json[key][8:])
    return json
