import trafaret as t

User = t.Dict({
  t.Key("id"): t.Int,
  t.Key("email"): t.String,
  t.Key("name"): t.String,
  t.Key("updatedAt"): t.String,
  t.Key("createdAt"): t.String,
  t.Key("token"): t.String,
  t.Key("tokenExpiresAt"): t.String,
  t.Key("refreshToken"): t.String,
  t.Key("role"): t.Int
})

EventNotApproved = t.Dict({
    t.Key("id"): t.Int,
    t.Key("serviceId"): t.String,
    t.Key("title"): t.String(allow_blank=True),
    t.Key("updatedAt"): t.String,
    t.Key("createdAt"): t.String,
    t.Key("description"): (t.String(allow_blank=True) | t.Null),
    t.Key("startTime"): (t.String(allow_blank=True) | t.Null),
    t.Key("endTime"): (t.String(allow_blank=True) | t.Null),
    t.Key("icon"): (t.String | t.Null),
    t.Key("originalIcon"): (t.String | t.Null),
    t.Key("city"): (t.String(allow_blank=True) | t.Null),
    t.Key("street"): (t.String(allow_blank=True) | t.Null),
    t.Key("lat"): (t.Float | t.Null),
    t.Key("lng"): (t.Float | t.Null),
    t.Key("url"): t.String(allow_blank=True),
    t.Key("serviceLogo"): t.String
})

EventsListNotApproved = t.Dict({
  t.Key("totalCount"): t.Int,
  t.Key("rows"): t.List(EventNotApproved)
})

EventsToSubcategories = {
  t.Key("subCategoryId"): t.Int,
  t.Key("eventId"): t.Int,
  t.Key("updatedAt"): t.String,
  t.Key("createdAt"): t.String
}

Subcategory = t.Dict({
  t.Key("id"): t.Int,
  t.Key("title"): t.String,
  t.Key("titleHe", optional=True): t.String,
  t.Key("updatedAt"): t.String,
  t.Key("createdAt"): t.String,
  t.Key("categoryId"): t.Int,
  t.Key("icon"): (t.String | t.Null),
  t.Key("eventsToSubCategories"): t.Dict(EventsToSubcategories)
})

Subcat = t.Dict({
  t.Key("id"): t.Int,
  t.Key("title"): t.String,
  t.Key("titleHe", optional=True): t.String,
  t.Key("updatedAt"): t.String,
  t.Key("createdAt"): t.String,
  t.Key("categoryId"): t.Int,
  t.Key("icon"): (t.String | t.Null),
})

Categories = t.List(t.Dict({
  t.Key("id"): t.Int,
  t.Key("title"): t.String,
  t.Key("icon"): (t.String | t.Null),
  t.Key("updatedAt"): t.String,
  t.Key("createdAt"): t.String,
  t.Key("subCategories"): t.List(Subcat)
}))

EventCategory = {
  t.Key("id"): t.Int,
  t.Key("title"): t.String,
  t.Key("titleHe", optional=True): t.String,
  t.Key("icon"): (t.String | t.Null),
  t.Key("updatedAt"): t.String,
  t.Key("createdAt"): t.String
}

Event = t.Dict({
    t.Key("id"): t.Int,
    t.Key("serviceId"): t.String,
    t.Key("title"): t.String(allow_blank=True),
    t.Key("updatedAt"): t.String,
    t.Key("createdAt"): t.String,
    t.Key("description"): (t.String(allow_blank=True) | t.Null),
    t.Key("startTime"): (t.String(allow_blank=True) | t.Null),
    t.Key("endTime"): (t.String(allow_blank=True) | t.Null),
    t.Key("icon"): (t.String | t.Null),
    t.Key("originalIcon"): (t.String | t.Null),
    t.Key("city"): (t.String(allow_blank=True) | t.Null),
    t.Key("street"): (t.String(allow_blank=True) | t.Null),
    t.Key("lat"): (t.Float | t.Null),
    t.Key("lng"): (t.Float | t.Null),
    t.Key("category"): (t.Dict(EventCategory) | t.Null),
    t.Key("url"): t.String(allow_blank=True),
    t.Key("subCategories"): t.List(Subcategory),
    t.Key("serviceLogo"): t.String
})


GetEvent = t.Dict({
    t.Key("totalCount"): t.Int,
    t.Key("rows"): t.List(t.Dict({
    t.Key("id"): t.Int,
    t.Key("serviceId"): t.String,
    t.Key("title"): t.String(allow_blank=True),
    t.Key("updatedAt"): t.String,
    t.Key("createdAt"): t.String,
    t.Key("description"): (t.String(allow_blank=True) | t.Null),
    t.Key("startTime"): (t.String(allow_blank=True) | t.Null),
    t.Key("endTime"): (t.String(allow_blank=True) | t.Null),
    t.Key("icon"): (t.String | t.Null),
    t.Key("originalIcon"): (t.String | t.Null),
    t.Key("city"): (t.String(allow_blank=True) | t.Null),
    t.Key("street"): (t.String(allow_blank=True) | t.Null),
    t.Key("lat"): (t.Float | t.Null),
    t.Key("lng"): (t.Float | t.Null),
    t.Key("category"): (t.Dict(EventCategory) | t.Null),
    t.Key("url"): t.String(allow_blank=True),
    t.Key("subCategories"): t.List(Subcategory)}))
})

EventsList = t.Dict({
  t.Key("totalCount"): t.Int,
  t.Key("rows"): t.List(Event)
})

EditedEvent = t.Dict({
    t.Key("icon", optional=True): t.String,
    t.Key("title", optional=True): t.String,
    t.Key("description", optional=True): t.String,
    t.Key("city", optional=True): t.String,
    t.Key("street", optional=True): t.String
})

ApprovedEvent = t.Dict({
    t.Key("id"): t.Int,
    t.Key("title"): t.String(allow_blank=True),
    t.Key("description"): (t.String(allow_blank=True) | t.Null),
    t.Key("serviceId"): t.Int,
    t.Key("icon"): (t.String | t.Null),
    t.Key("originalIcon"): (t.String | t.Null),
    t.Key("startTime"): (t.String(allow_blank=True) | t.Null),
    t.Key("endTime"): (t.String(allow_blank=True) | t.Null),
    t.Key("city"): (t.String(allow_blank=True) | t.Null),
    t.Key("street"): (t.String(allow_blank=True) | t.Null),
    t.Key("lat"): (t.Float | t.Null),
    t.Key("lng"): (t.Float | t.Null),
    t.Key("updatedAt"): t.String,
    t.Key("createdAt"): t.String,
    t.Key("url"): t.String(allow_blank=True)
})

FavoritesList = t.Dict({
  t.Key("count"): t.Int,
  t.Key("rows"): t.List(
    t.Dict({
      t.Key("categoryId"): t.Int,
      t.Key("isDeleted"): t.Int,
      t.Key("isBlocked"): t.Int,
      t.Key("serviceTypeId"): t.Int,
      t.Key("created_at"): t.String,
      t.Key("updated_at"): t.String,
      t.Key("status"): t.Int,
      t.Key("id"): t.Int,
      t.Key("title"): t.String(allow_blank=True),
      t.Key("description"): (t.String(allow_blank=True) | t.Null),
      t.Key("serviceId"): t.Int,
      t.Key("icon"): (t.String | t.Null),
      t.Key("originalIcon"): (t.String | t.Null),
      t.Key("startTime"): (t.String(allow_blank=True) | t.Null),
      t.Key("endTime"): (t.String(allow_blank=True) | t.Null),
      t.Key("city"): (t.String(allow_blank=True) | t.Null),
      t.Key("street"): (t.String(allow_blank=True) | t.Null),
      t.Key("lat"): (t.Float | t.Null),
      t.Key("lng"): (t.Float | t.Null),
      t.Key("updatedAt"): t.String,
      t.Key("createdAt"): t.String,
      t.Key("url"): t.String(allow_blank=True)
     })
  )
})

ReportReasons = t.List(
    t.Dict({
    t.Key("id"): t.Int,
    t.Key("text"): t.String,
    t.Key("createdAt"): t.String,
    t.Key("updatedAt"): t.String
}
)
)

ReportedEvents = t.Dict({
  t.Key("totalCount"): t.Int,
  t.Key("rows"): t.List(t.Dict({
      t.Key("id"): t.Int,
      t.Key("title"): t.String(allow_blank=True),
      t.Key("description"): t.String(allow_blank=True),
      t.Key("icon"): (t.String(allow_blank=True) | t.Null),
      t.Key("originalIcon"): (t.String(allow_blank=True) | t.Null),
      t.Key("url"): t.String(allow_blank=True),
      t.Key("serviceLogo"): t.String,
      t.Key("startTime"): t.String,
      t.Key("endTime"): t.String,
      t.Key("reports"): t.List(
          t.Dict({
            t.Key("id"): t.Int,
            t.Key("text"): t.String
          }))
  }))
})

ReportedEvent =t.Dict({
    t.Key("id"): t.Int,
    t.Key("userId"): t.Int,
    t.Key("eventId"): t.Int,
    t.Key("reasonId"): t.Int
})



