# Created by dudnik_anna_cr at 28.11.16
Feature: Get approved events

   @test_01
  Scenario:  Positive. Admin gets approved events
    Given I set "Content-Type" header to "application/json"
      And I set "Authorization" header to "context.admin_token"
    When I make a GET request to "/events/approved"
    Then the response status code should equal 200
     And the response structure should equal "EventsList"


     @test_01
  Scenario: Negative. User gets approved events
    Given I set "Content-Type" header to "application/json"
     And I set "Authorization" header to "context.user_token"
    When I make a GET request to "/events/approved"
    Then the response status code should equal 405
