# Created by dudnik_anna_cr at 25.11.16
Feature: Authorization

   @test_01
  Scenario: User authorization
    Given I run sql script
     """
     context.refresh = SELECT refresh_token FROM users WHERE id = 28;
     """
     And I set "Content-Type" header to "application/json"
     And I add request body
     """
     {
     "refreshToken": "context.refresh"
     }
     """
    When I make a POST request to "/signin/refresh"
    Then the response status code should equal 200
    And the response structure should equal "User"
     And JSON at path ".id" should equal 28
     And JSON at path ".email" should equal "110122159475221@facebook.com"
     And JSON at path ".name" should equal "Sonya Koshkina"
     And JSON value at path ".token" I save as "context.user_token"
     And JSON at path ".role" should equal 0


     @test_01
  Scenario: Admin authorization
    Given I set "Content-Type" header to "application/json"
      And I add request body
     """
     {
     "email": "admin@admin.com",
     "password": "adminadmin"
     }
     """
    When I make a POST request to "/signin/basic"
    Then the response status code should equal 200
    And the response structure should equal "User"
     And JSON at path ".id" should equal 1
     And JSON at path ".email" should equal "admin@admin.com"
     And JSON at path ".name" should equal "admin"
     And JSON value at path ".token" I save as "context.admin_token"
     And JSON at path ".role" should equal 2
